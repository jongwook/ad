## Simple Automatic Differentiation Library

Three header files in this project implements [automatic differentiation](http://en.wikipedia.org/wiki/Automatic_differentiation).

* `ad.h` : common codes. include this file or bad things will happen
* `forward.h` : forward-mode implementation for R^n -> R functions
* `backward.h` : backward-mode implementation for R^n -> R functions

It is quite unpolished yet and may contain a lot of bugs; anyone is welcome to contribute.

## Usage

Since this library is template based, there is no library to link with.

Just copy all the header files and include `ad.h`.

## Running Examples

The cpp files contain examples for forward and backward mode operations.

Try `make` and see whether it goes like this:

	$ make
	g++ -O3 -Wall backward-example.cpp -o backward-example
	g++ -O3 -Wall forward-example.cpp -o forward-example
	$ ./forward-example 
	f(x,y) = sqrt(x)*y + sin(sqrt(x))
	f(1,2) = 2.84147
	fx(1,2) = 1.27015
	fy(1,2) = 1
	f(1,2) = 2.84147
	fx(1,2) = 1.27015
	fy(1,2) = 1
	$ ./backward-example 
	f(x,y) = sqrt(x)*y + sin(sqrt(x))
	f(1,2) = 2.84147
	fx(1,2) = 1.27015
	fy(1,2) = 1
	$ 

## Template Function

It is convenient for automatic differentiation if you make a template function, as follows.

	template <typename T>
	T func(T &x, T &y) {
		T z = sqrt(x);
		return z*y + sin(z);
	}

This function calculates `sqrt(x)*y + sin(sqrt(x))`, and works with any types that have sqrt(), sin() and operator +, * defined.

## Forward mode 

For forward mode automatic differentiation, a template class called `FADScalar` will be used.

`FADdouble` is a shorthand for `FADScalar<double>` and `FADfloat` is for `FADScalar<float>`.

In forward mode, you need to set the number and order of independent variables before calling the function.

	FADdouble x = 1.0, y = 2.0;
	x.set(0,2);	// variable 0 among total 2 
	y.set(1,2); // variable 1 among total 2 
	FADdouble f = func(x, y);
	
	cout << "f(1,2) = " << f.value() << endl;
	cout << "fx(1,2) = " << f.diff(0) << endl;
	cout << "fy(1,2) = " << f.diff(1) << endl;

As with most other programming languages, the indices start from zero.

Alternatively, the total number of independent variables can be specified as a template argument, as follows.

	FADScalar<double, 2> x = 1.0, y = 2.0;
	x.set(0);
	y.set(1);
	FADScalar<double, 2> f = func(x, y);
	
	cout << "f(1,2) = " << f.value() << endl;
	cout << "fx(1,2) = " << f.diff(0) << endl;
	cout << "fy(1,2) = " << f.diff(1) << endl;

In this case, the implementation uses the stack memory for faster calculation.

## Backward mode

Backward mode uses 'ADScalar', and 'ADdouble' and 'ADfloat' are shorthands.

	ADdouble x = 1.0, y = 2.0;
	ADdouble f = func(x, y);
	f.run();

	cout << "f(1,2) = " << f.value() << endl;
	cout << "fx(1,2) = " << x.diff() << endl;
	cout << "fy(1,2) = " << y.diff() << endl;
	return 0;

Unlike forward mode, the partial differentials are stored in the independent variables. 

## License

MIT License

## Author

Jong Wook Kim (jongwook at nyu.edu)

