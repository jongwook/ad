
#ifndef __ad_ad_h__
#define __ad_ad_h__

#include <cmath>
#include <cstdlib>
#include <iostream>

#ifndef NULL
#define NULL 0
#endif

#define AD_ASSERT(cond, message) \
	if(!(cond)) { \
		std::cerr << "ASSERTION FAILED(" << __FILE__ << ":" << __LINE__ << ") " << (message) << std::endl; \
		exit(-1); \
	}

// Operations
enum ADOpcode {
	AD_NONE = 0,
	AD_ADD,  // addition
	AD_SUB,  // subtraction
	AD_MUL,  // multiplication
	AD_DIV,  // division
	AD_POS,  // unary + operator
	AD_NEG,  // unary - operator
	AD_POW,  // power; pow(x,y)
	AD_SQRT, // square root
	AD_LOG,  // natural logarithm
	AD_EXP,  // exponential
	AD_SIN,  // sine
	AD_COS,  // cosine
	AD_TAN,  // tangent
	AD_ASIN, // arcsine
	AD_ACOS, // arccos
	AD_ATAN, // arctan
};

// include forward AD implementation
#include "forward.h"

// include backward AD implementation
#include "backward.h"

#endif

