
#ifndef __ad_backward_h__
#define __ad_backward_h__

#include <vector>
#include <new>

template <typename T>
class ADScalar;

template <typename T>
class ADFunction;

template <unsigned int ELEMENT_SIZE>
class ADFixedAllocator {
public:
	// how many elements a block contains
	static const unsigned int ELEMENTS_IN_BLOCK = 10240;
	static const unsigned int BLOCK_SIZE = ELEMENT_SIZE * ELEMENTS_IN_BLOCK;
	
	// this type has size 1
	typedef unsigned char byte_type;
	typedef std::size_t size_type;
	
	struct Blocks {
		std::vector<byte_type *> ptrs;
		
		~Blocks() {
			for(std::size_t i=0; i<ptrs.size(); i++) {
				delete [] ptrs[i];
			}
		}
	};
	
	static void* allocate() {
		// insert a new block if needed
		
		++_allocated;
		
		if(_free_list) {
			void *result = _free_list;
			_free_list = reinterpret_cast<void *>(*(size_type*)_free_list);
			return result;
		}
		
		if(_head_index == BLOCK_SIZE) {
			_blocks.ptrs.push_back(new byte_type[BLOCK_SIZE]);
			_head_index = 0;
		}
		
		if(_blocks.ptrs.empty()) {
			_blocks.ptrs.reserve(1024);
			_blocks.ptrs.push_back(new byte_type[BLOCK_SIZE]);
		}
		
		void *result = &(_blocks.ptrs.back()[_head_index]);
		_head_index += ELEMENT_SIZE;
		
		return result;
	}
	
	static void deallocate(void *ptr) { 
		if(ptr != NULL) {
			*(size_type*)ptr = reinterpret_cast<size_type>(_free_list);
			_free_list = ptr;
			if(--_allocated == 0) {
				freeAll();
			}
		}		
	}
	
private:
	static void freeAll() {
		// free all except the first block;
		for(std::size_t i=1; i<_blocks.ptrs.size(); i++) {
			delete [] _blocks.ptrs[i];
		}
		_blocks.ptrs.resize(1);
		_head_index = 0;
		_free_list = NULL;
	}
	
	// set of pointers to the blocks
	static Blocks _blocks;
	static size_type _head_index;
	static size_type _allocated;	
	static void* _free_list;
};

template <unsigned int ELEMENT_SIZE>
typename ADFixedAllocator<ELEMENT_SIZE>::Blocks ADFixedAllocator<ELEMENT_SIZE>::_blocks;

template <unsigned int ELEMENT_SIZE>
typename ADFixedAllocator<ELEMENT_SIZE>::size_type ADFixedAllocator<ELEMENT_SIZE>::_head_index = 0;

template <unsigned int ELEMENT_SIZE>
typename ADFixedAllocator<ELEMENT_SIZE>::size_type ADFixedAllocator<ELEMENT_SIZE>::_allocated = 0;

template <unsigned int ELEMENT_SIZE>
void* ADFixedAllocator<ELEMENT_SIZE>::_free_list = NULL;



template <typename T>
class ADAllocator {
public:
	typedef std::size_t size_type;
	typedef std::ptrdiff_t difference_type;
	typedef T* pointer;
	typedef const T* const_pointer;
	typedef T& reference;
	typedef const T& const_reference;
	typedef T value_type;
	
	pointer address(reference ref) const { return &ref; }
	const_pointer address(const_reference ref) const { return &ref; }
	
	template<class U>
	struct rebind {
		typedef ADAllocator<U> other;
	};
	
	ADAllocator() throw() {}
	
	template<class U>
	ADAllocator(const ADAllocator<T>&) throw() {}
	
	template<class U>
	ADAllocator& operator= (const ADAllocator<U>&) { return *this; }
	
	pointer allocate(size_type count, const void* = 0) {
		AD_ASSERT(count == 1, "Array allocation not supported");
		return static_cast<pointer>(ADFixedAllocator<sizeof(T)>::allocate());
	}
	
	void deallocate(pointer ptr, size_type) {
		ADFixedAllocator<sizeof(T)>::deallocate(ptr);
	}
	
	void construct(pointer ptr, const T &val) {
		new ((void*)ptr) T(val);
	}
	
	void destroy(pointer ptr) {
		ptr->T::~T();
	}
};

template <typename T>
struct ADData {
	ADData(T value = T()) : opcode(AD_NONE), value(value), diff(T()), op1(NULL), op2(NULL), parent(NULL) {}
	ADOpcode opcode;
	T value, diff;
	ADData<T> *op1, *op2;
	ADFunction<ADScalar<T> > *parent;
	
	void* operator new (std::size_t size) throw(std::bad_alloc) {
		return ADAllocator<ADData<T> >().allocate(1);
	}
	
	void operator delete (void *ptr) throw() {
		ADAllocator<ADData<T> >().deallocate(static_cast<typename ADAllocator<ADData<T> >::pointer>(ptr), 1);
	}
};

template <typename T>
struct ADScalarInfo { typedef T raw_type; };

template <typename T>
struct ADScalarInfo<ADScalar<T> > { typedef T raw_type; };

template <typename T>
void DerivativeAt(ADOpcode opcode, T x, T y, T &dx, T &dy);

// singleton class for memory management
template <typename T>
class ADFunction {
public:
	typedef typename ADScalarInfo<T>::raw_type raw_type;
	typedef ADData<raw_type> data_type;
	
	// conversion from ADscalar; AD happens here
	ADFunction(const ADScalar<raw_type> &scalar) {
		data_type *top = scalar.data();
		
		// if a scalar has no data, it is a constant;
		if(top == NULL) return;
		
		top->diff = 1.0f;
		sort_terms(top);
		
		for(typename std::vector<data_type*>::reverse_iterator it = _data.rbegin(); it != _data.rend(); it++) {
			data_type *data = *it;
			
			ADOpcode opcode = data->opcode;
			data_type *op1 = data->op1;
			data_type *op2 = data->op2;
			raw_type diff = data->diff, d1, d2;
			DerivativeAt(opcode, (op1 != NULL) ? op1->value : 0.0f, (op2 != NULL) ? op2->value : 0.0f, d1, d2);
			if(data->op1 != NULL) data->op1->diff += d1 * diff;
			if(data->op2 != NULL) data->op2->diff += d2 * diff;
		}
	}
	
	~ADFunction() {
		for(typename std::vector<data_type*>::iterator it = _data.begin(); it != _data.end(); it++) {
			data_type *data = *it;
			
			if(data->parent == this) {
				// this class is responsible for releasing this data
				delete data;
			}
		}
	}

	raw_type value() {
		AD_ASSERT(_data.size() > 0, "Function has no data");
		return _data.back()->value; 
	}
	
	operator raw_type() {
		return value();
	}
	
	operator T() {
		return T(value());
	}
	
	static ADFunction<T>* const not_assigned;
	
private:
	std::vector<data_type*> _data;
	static ADFunction<T> _not_assigned;
	
	void sort_terms(data_type *data) {
		// topological sort terms via DFS
		if(data->parent == not_assigned) {
			data->parent = this;
		}
		if(data->op1 && data->op1->parent != this) {
			sort_terms(data->op1);
		}
		if(data->op2 && data->op2->parent != this) {
			sort_terms(data->op2);
		}
		_data.push_back(data);
	}
};

template <typename T>
ADFunction<T> ADFunction<T>::_not_assigned = ADFunction<T>(ADScalar<typename ADFunction<T>::raw_type>(0.0f));

template <typename T>
ADFunction<T>* const ADFunction<T>::not_assigned = &ADFunction<T>::_not_assigned;

template <typename T>
class ADScalar {
public: 
	typedef T value_type;
	
	ADScalar() : _value(0), _data(new ADData<T>()) {
	}
	
	// conversion from scalar type
	ADScalar(const T &value) : _value(value), _data(new ADData<T>()) { 
	}

	// conversion from integer type
	ADScalar(int value) : _value(value), _data(NULL) {
	}
	
	ADScalar<T>& operator=(const T &value) {
		_value = value;
		if(_data != NULL) {
			_data->value = value;
		}
		return *this;
	}

private:
	// unary operator initialization	
	ADScalar(ADOpcode opcode, const ADScalar<T> *op1, const ADScalar<T> *op2 = NULL) {
		_data = new ADData<T>;
		_data->opcode = opcode;
		_data->op1 = (op1 == NULL) ? NULL : op1->data();
		_data->op2 = (op2 == NULL) ? NULL : op2->data();
		_data->parent = ADFunction<ADScalar<T> >::not_assigned;
		
		switch(opcode) {
			case AD_NONE: break;
			case AD_ADD:  _value = _data->value = op1->value() + op2->value(); break;
			case AD_SUB:  _value = _data->value = op1->value() - op2->value(); break;
			case AD_MUL:  _value = _data->value = op1->value() * op2->value(); break;
			case AD_DIV:  _value = _data->value = op1->value() / op2->value(); break;
			case AD_POS:  _value = _data->value = +op1->value(); break;
			case AD_NEG:  _value = _data->value = -op1->value(); break;
			case AD_POW:  _value = _data->value = pow(op1->value(), op2->value()); break;
			case AD_SQRT: _value = _data->value = sqrt(op1->value()); break;
			case AD_LOG:  _value = _data->value = log(op1->value()); break;
			case AD_EXP:  _value = _data->value = exp(op1->value()); break;
			case AD_SIN:  _value = _data->value = sin(op1->value()); break;
			case AD_COS:  _value = _data->value = cos(op1->value()); break;
			case AD_TAN:  _value = _data->value = tan(op1->value()); break;
			case AD_ASIN: _value = _data->value = asin(op1->value()); break;
			case AD_ACOS: _value = _data->value = acos(op1->value()); break;
			case AD_ATAN: _value = _data->value = atan(op1->value()); break;
			default:      AD_ASSERT(false, "should not reach here");
		}
	}

public:
	// accessors
	inline T value() const { return _value; }
	inline T diff() const { return data()->diff; }
	inline ADData<T>* data() const { return _data; }
	
	// opeartor new; for checking heap allocation
	// TODO
	
	// overloaded operators
	ADScalar<T>  operator+ (const ADScalar<T> &other) const { return ADScalar<T>(AD_ADD, this, &other); }
	ADScalar<T>  operator- (const ADScalar<T> &other) const { return ADScalar<T>(AD_SUB, this, &other); }
	ADScalar<T>  operator* (const ADScalar<T> &other) const { return ADScalar<T>(AD_MUL, this, &other); }
	ADScalar<T>  operator/ (const ADScalar<T> &other) const { return ADScalar<T>(AD_DIV, this, &other); }

	ADScalar<T>& operator+=(const ADScalar<T> &other) { return (*this) = (*this) + other; }
	ADScalar<T>& operator-=(const ADScalar<T> &other) { return (*this) = (*this) - other; }
	ADScalar<T>& operator*=(const ADScalar<T> &other) { return (*this) = (*this) * other; }
	ADScalar<T>& operator/=(const ADScalar<T> &other) { return (*this) = (*this) / other; }

	ADScalar<T>  operator+ () const                   { return ADScalar<T>(AD_POS, this); }
	ADScalar<T>  operator- () const                   { return ADScalar<T>(AD_NEG, this); }

	friend ADScalar<T> pow (const ADScalar<T> &op1, const ADScalar<T> &op2) { return ADScalar<T>(AD_POW, &op1, &op2); }

	friend ADScalar<T> sqrt(const ADScalar<T> &op1) { return ADScalar<T>(AD_SQRT, &op1); }
	friend ADScalar<T> log (const ADScalar<T> &op1) { return ADScalar<T>(AD_LOG , &op1); }
	friend ADScalar<T> exp (const ADScalar<T> &op1) { return ADScalar<T>(AD_EXP , &op1); }

	friend ADScalar<T> sin (const ADScalar<T> &op1) { return ADScalar<T>(AD_SIN , &op1); }
	friend ADScalar<T> cos (const ADScalar<T> &op1) { return ADScalar<T>(AD_COS , &op1); }
	friend ADScalar<T> tan (const ADScalar<T> &op1) { return ADScalar<T>(AD_TAN , &op1); }
	friend ADScalar<T> asin(const ADScalar<T> &op1) { return ADScalar<T>(AD_ASIN, &op1); }
	friend ADScalar<T> acos(const ADScalar<T> &op1) { return ADScalar<T>(AD_ACOS, &op1); }
	friend ADScalar<T> atan(const ADScalar<T> &op1) { return ADScalar<T>(AD_ATAN, &op1); }


protected:
	T _value;
	ADData<T> *_data;
};

template <typename T>
void DerivativeAt(ADOpcode opcode, T x, T y, T &dx, T &dy) {
	T tmp, logx;
	switch(opcode) {
		case AD_NONE:
			break;
		case AD_ADD:
			// d(x+y) = 1 dx + 1 dy
			dx = 1.0f;
			dy = 1.0f;
			break;
		case AD_SUB:
			// d(x-y) = 1 dx - 1 dy
			dx = 1.0f;
			dy = -1.0f;
			break;
		case AD_MUL:
			// d(x*y) = y dx + x dy
			dx = y;
			dy = x;
			break;
		case AD_DIV:
			// d(x/y) = 1/y dx - x/y^2 dy
			dx = 1.0f/y;
			dy = -x/(y*y);
			break;
		case AD_POS:
			// d(+x) = dx
			dx = 1.0f;
			break;
		case AD_NEG: 
			// d(-y) = -1 dx
			dx = -1.0f;
			break;
		case AD_POW:
			// d(x^y) = y * x^y / x dx + x^y * log(x) dy
			tmp = (T)pow(x,y);
			logx = (x != 0.0f) ? (T)log(x) : 0.0f;
			dx = y * tmp / x;
			dy = tmp * logx;
			break;
		case AD_SQRT:
			// dsqrt(x) = 1/(2*sqrt(x)) dx
			dx = 1.0f / (2 * (T)sqrt(x));
			break;
		case AD_LOG:
			// dlog(x) = 1/x dx
			dx = 1.0 / x;
			break;
		case AD_EXP:
			// dexp(x) = exp(x) dx
			dx = (T)exp(x);
			break;
		case AD_SIN:
			// dsin(x) = cos(x) dx
			dx = (T)cos(x);
			break;
		case AD_COS:
			// dcos(x) = -sin(x) dx
			dx = -(T)sin(x);
			break;
		case AD_TAN:
			// dtan(x) = sec(x)^2 dx = 1/cos(x)^2 dx
			tmp = (T)cos(x);
			dx = -1.0f/(tmp*tmp);
			break;
		case AD_ASIN:
			// dasin(x) = 1/sqrt(1-x^2) dx
			dx = 1.0f / (T)sqrt(1.0f - x*x);
			break;
		case AD_ACOS:
			// dacos(x) = -1/sqrt(1-x^2) dx
			dx = -1.0f / (T)sqrt(1.0f - x*x);
			break;
		case AD_ATAN:
			// datan(x)/dx = 1/(1+x^2)
			dx = 1.0f / (1.0f + x*x);
			break;
	}

}

typedef ADScalar<double> ADdouble;
typedef ADScalar<float> ADfloat;


#endif

