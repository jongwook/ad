	// AD Example

#include <iostream>
using namespace std;

#include "ad.h"

template <typename T>
ADFunction<T> func(T &x, T &y) {
	T z = sqrt(x);
	return z*y + sin(z);
}

int main() {
	ADdouble x = 1.0, y = 2.0;
	ADdouble f = func(x, y);

	cout << "f(x,y) = sqrt(x)*y + sin(sqrt(x))" << endl;
	cout << "f(1,2) = " << f.value() << endl;
	cout << "fx(1,2) = " << x.diff() << endl;
	cout << "fy(1,2) = " << y.diff() << endl;

	return 0;
}

