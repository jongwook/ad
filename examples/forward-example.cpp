// AD Example

#include <iostream>
using namespace std;

#include "ad.h"

template <typename T>
T func(T &x, T &y) {
	T z = sqrt(x);
	return z*y + sin(z);
}

int main() {
	cout << "f(x,y) = sqrt(x)*y + sin(sqrt(x))" << endl;
	{
		FADdouble x = 1.0, y = 2.0;
		x.set(0,2);
		y.set(1,2);
		FADdouble f = func(x, y);

		cout << "f(1,2) = " << f.value() << endl;
		cout << "fx(1,2) = " << f.diff(0) << endl;
		cout << "fy(1,2) = " << f.diff(1) << endl;
	}
	{
		FADScalar<double, 2> x = 1.0, y = 2.0;
		x.set(0);
		y.set(1);
		FADScalar<double, 2> f = func(x, y);

		cout << "f(1,2) = " << f.value() << endl;
		cout << "fx(1,2) = " << f.diff(0) << endl;
		cout << "fy(1,2) = " << f.diff(1) << endl;
	}
	return 0;
}

