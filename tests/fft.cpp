
#include <iostream>
#include <complex>
#include <cstdlib>

using namespace std;

#include "ad.h"
#include "kissfft.h"

#include "badiff.h"

#define DATA_SIZE 8192
#define LOOP_COUNT 10

#include <sys/time.h>

static double now_ms(void) 
{ 
	struct timeval tv; 
	gettimeofday(&tv, NULL); 
	return tv.tv_sec*1000. + tv.tv_usec/1000.; 
}

template<typename T>
T fftsum(T data[]) {
	complex<T> input[DATA_SIZE], output[DATA_SIZE];
	for(int i=0; i<DATA_SIZE; i++) {
		input[i] = data[i];
	}
	
	kissfft<T> fft(DATA_SIZE);
	fft.transform(input, output);
	
	T total = 0.0;
	
	for(int i=0; i<DATA_SIZE; i++) {
		total += norm(output[i]);
	}
	
	return total;
}

int main() {
	// raw fft
	
	double original[DATA_SIZE];
	
	for(int i=0; i<DATA_SIZE; i++) {
		original[i] = (double)rand()/RAND_MAX;
	}
	
	double start = now_ms();
	
	for(int i=0; i<LOOP_COUNT; i++) {
		volatile double total = fftsum(original);
	}
	
	double elapsed = now_ms() - start;
	
	cout << "elapsed : " << elapsed << endl;
	
	
	// my version
	
	ADdouble autodiff[DATA_SIZE];
	
	for(int i=0; i<DATA_SIZE; i++) {
		autodiff[i] = original[i];
	}
	
	double ad_start = now_ms();
	double ad_sum = 0;
	
	for(int i=0; i<LOOP_COUNT; i++) {
		ADFunction<ADdouble> sum = fftsum(autodiff);
		ad_sum += autodiff[0].diff();
	}
	
	double ad_elapsed = now_ms() - ad_start;
	
	cout << "ad elapsed : " << ad_elapsed << "\tsum : " << ad_sum << endl;

	
	
	// fadbad
	
	fadbad::B<double> fb[DATA_SIZE];
	
	for(int i=0; i<DATA_SIZE; i++) {
		fb[i] = original[i];
	}
	
	double fadbad_start = now_ms();
	double fadbad_sum = 0;
	
	for(int i=0; i<LOOP_COUNT; i++) {
		fadbad::B<double> sum = fftsum(fb);
		sum.diff(0,1);
		fadbad_sum += fb[0].d(0);
	}
	
	double fadbad_elapsed = now_ms() - fadbad_start;
	
	cout << "fadbad elapsed : " << fadbad_elapsed << "\tsum : " << fadbad_sum << endl;
	
	return 0;
}


