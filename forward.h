
#ifndef __ad_forward_h__
#define __ad_forward_h__

#include <iostream>

template <typename T, unsigned int N=0>
class FADScalar;

template <typename T, unsigned int M>
void ForwardDifferentiate(unsigned int N, ADOpcode opcode, const FADScalar<T, M> &op, T &value, T*diff);

template <typename T, unsigned int M>
void ForwardDifferentiate(unsigned int N, ADOpcode opcode, const FADScalar<T, M> &op1, const FADScalar<T, M> &op2, T &value, T *diff);

// stack implementation
template <typename T, unsigned int N>
class FADScalar {
public: 
	FADScalar() {}
	
	// conversion from scalar type
	FADScalar(const T &value) { _value = value; }

	// unary operator initialization	
	FADScalar(ADOpcode opcode, const FADScalar &op) {
		ForwardDifferentiate(N, opcode, op, _value, _diff);
	}

	// binary operator initialization
	FADScalar(ADOpcode opcode, const FADScalar &op1, const FADScalar &op2) {
		ForwardDifferentiate(N, opcode, op1, op2, _value, _diff);
	}

	// accessors
	inline T value() const { return _value; }
	inline T diff(int index) const { return _diff[index]; }

	// set as a dependent variable
	inline void set(unsigned int index) {
		unsigned int i;
		for(i=0; i<index; i++) {
			_diff[i] = 0.0f;
		}
		_diff[index] = 1.0f;
		for(i=index+1; i<N; i++) {
			_diff[i] = 0.0f;
		}
	}
	
	// overloaded operators
	FADScalar<T, N>  operator+ (const FADScalar<T, N> &other) { return FADScalar<T, N>(AD_ADD, *this, other); }
	FADScalar<T, N>  operator- (const FADScalar<T, N> &other) { return FADScalar<T, N>(AD_SUB, *this, other); }
	FADScalar<T, N>  operator* (const FADScalar<T, N> &other) { return FADScalar<T, N>(AD_MUL, *this, other); }
	FADScalar<T, N>  operator/ (const FADScalar<T, N> &other) { return FADScalar<T, N>(AD_DIV, *this, other); }

	FADScalar<T, N>& operator+=(const FADScalar<T, N> &other) { return (*this) = (*this) + other; }
	FADScalar<T, N>& operator-=(const FADScalar<T, N> &other) { return (*this) = (*this) - other; }
	FADScalar<T, N>& operator*=(const FADScalar<T, N> &other) { return (*this) = (*this) * other; }
	FADScalar<T, N>& operator/=(const FADScalar<T, N> &other) { return (*this) = (*this) / other; }

	FADScalar<T, N>  operator+ ()			 { return FADScalar<T, N>(AD_POS, *this); }
	FADScalar<T, N>  operator- ()			 { return FADScalar<T, N>(AD_NEG, *this); }

	friend FADScalar<T, N> pow (const FADScalar<T, N> &op1, const FADScalar<T, N> &op2) { return FADScalar<T, N>(AD_POW, op1, op2); }

	friend FADScalar<T, N> sqrt(const FADScalar<T, N> &op1) { return FADScalar<T, N>(AD_SQRT, op1); }
	friend FADScalar<T, N> log (const FADScalar<T, N> &op1) { return FADScalar<T, N>(AD_LOG , op1); }
	friend FADScalar<T, N> exp (const FADScalar<T, N> &op1) { return FADScalar<T, N>(AD_EXP , op1); }

	friend FADScalar<T, N> sin (const FADScalar<T, N> &op1) { return FADScalar<T, N>(AD_SIN , op1); }
	friend FADScalar<T, N> cos (const FADScalar<T, N> &op1) { return FADScalar<T, N>(AD_COS , op1); }
	friend FADScalar<T, N> tan (const FADScalar<T, N> &op1) { return FADScalar<T, N>(AD_TAN , op1); }
	friend FADScalar<T, N> asin(const FADScalar<T, N> &op1) { return FADScalar<T, N>(AD_ASIN, op1); }
	friend FADScalar<T, N> acos(const FADScalar<T, N> &op1) { return FADScalar<T, N>(AD_ACOS, op1); }
	friend FADScalar<T, N> atan(const FADScalar<T, N> &op1) { return FADScalar<T, N>(AD_ATAN, op1); }


protected:
	T _value;
	T _diff[N];
};

// heap implementation
template <typename T>
class FADScalar<T, 0> {
public: 
	FADScalar() : _diff(NULL), _N(0) {}
	
	// conversion from scalar type
	FADScalar(const T &value) : _diff(NULL), _N(0) { _value = value; }

	// unary operator initialization	
	FADScalar(ADOpcode opcode, const FADScalar<T> &op) : _diff(NULL), _N(0) {
		_N = op.N();
		_diff = new T[_N];
		ForwardDifferentiate(_N, opcode, op, _value, _diff);
	}

	// binary operator initialization
	FADScalar(ADOpcode opcode, const FADScalar<T> &op1, const FADScalar<T> &op2) : _diff(NULL), _N(0) {
		AD_ASSERT(op1.N() == op2.N(), "inconsistent size");
		_N = op1.N();
		_diff = new T[_N];
		ForwardDifferentiate(_N, opcode, op1, op2, _value, _diff);
	}

	~FADScalar() {
		delete [] _diff;
	}
	
	// accessors
	inline T value() const { return _value; }
	inline T diff(int index) const { return _diff[index]; }
	inline unsigned int N() const { return _N; }

	// set as a dependent variable
	inline void set(unsigned int index, unsigned int N) {
		if(_diff == NULL) {
			_diff = new T[N];
			_N = N;
		} else {
			AD_ASSERT(_N == N, "inconsistent size");
		}
		unsigned int i;
		for(i=0; i<index; i++) {
			_diff[i] = 0.0f;
		}
		_diff[index] = 1.0f;
		for(i=index+1; i<N; i++) {
			_diff[i] = 0.0f;
		}
	}
	
	// overloaded operators
	FADScalar<T>  operator+ (const FADScalar<T> &other) { return FADScalar<T>(AD_ADD, *this, other); }
	FADScalar<T>  operator- (const FADScalar<T> &other) { return FADScalar<T>(AD_SUB, *this, other); }
	FADScalar<T>  operator* (const FADScalar<T> &other) { return FADScalar<T>(AD_MUL, *this, other); }
	FADScalar<T>  operator/ (const FADScalar<T> &other) { return FADScalar<T>(AD_DIV, *this, other); }

	FADScalar<T>& operator+=(const FADScalar<T> &other) { return (*this) = (*this) + other; }
	FADScalar<T>& operator-=(const FADScalar<T> &other) { return (*this) = (*this) - other; }
	FADScalar<T>& operator*=(const FADScalar<T> &other) { return (*this) = (*this) * other; }
	FADScalar<T>& operator/=(const FADScalar<T> &other) { return (*this) = (*this) / other; }

	FADScalar<T>  operator+ ()			 { return FADScalar<T>(AD_POS, *this); }
	FADScalar<T>  operator- ()			 { return FADScalar<T>(AD_NEG, *this); }

	friend FADScalar<T> pow (const FADScalar<T> &op1, const FADScalar<T> &op2) { return FADScalar<T>(AD_POW, &op1, &op2); }

	friend FADScalar<T> sqrt(const FADScalar<T> &op1) { return FADScalar<T>(AD_SQRT, op1); }
	friend FADScalar<T> log (const FADScalar<T> &op1) { return FADScalar<T>(AD_LOG , op1); }
	friend FADScalar<T> exp (const FADScalar<T> &op1) { return FADScalar<T>(AD_EXP , op1); }

	friend FADScalar<T> sin (const FADScalar<T> &op1) { return FADScalar<T>(AD_SIN , op1); }
	friend FADScalar<T> cos (const FADScalar<T> &op1) { return FADScalar<T>(AD_COS , op1); }
	friend FADScalar<T> tan (const FADScalar<T> &op1) { return FADScalar<T>(AD_TAN , op1); }
	friend FADScalar<T> asin(const FADScalar<T> &op1) { return FADScalar<T>(AD_ASIN, op1); }
	friend FADScalar<T> acos(const FADScalar<T> &op1) { return FADScalar<T>(AD_ACOS, op1); }
	friend FADScalar<T> atan(const FADScalar<T> &op1) { return FADScalar<T>(AD_ATAN, op1); }


protected:
	T _value;
	T *_diff;
	unsigned int _N;
};


template <typename T, unsigned int M>
void ForwardDifferentiate(unsigned int N, ADOpcode opcode, const FADScalar<T, M> &op, T &value, T*diff) {
	T tmp, opvalue;
	switch(opcode) {
		case AD_POS:
			value = +op.value(); 
			for(unsigned int i=0; i<N; i++) {
				diff[i] = op.diff(i);
			}
			break;
		case AD_NEG: 
			value = -op.value(); 
			for(unsigned int i=0; i<N; i++) {
				diff[i] = -op.diff(i);
			}
			break;
		case AD_SQRT:
			// dsqrt(x)/dx = 1/(2*sqrt(x))
			value = (T)sqrt(op.value()); 
			tmp = 1.0f / (2.0f * value);
			for(unsigned int i=0; i<N; i++) {
				diff[i] = op.diff(i) * tmp;
			}
			break;
		case AD_LOG:
			// dlog(x)/dx = 1/x
			opvalue = op.value();
			value = (T)log(opvalue); 
			for(unsigned int i=0; i<N; i++) {
				diff[i] = op.diff(i) / opvalue;
			}
			break;
		case AD_EXP:
			// dexp(x)/dx = exp(x)
			value = exp(op.value());
			for(unsigned int i=0; i<N; i++) {
				diff[i] = op.diff(i) * value;
			}
			break;
		case AD_SIN:
			// dsin(x)/dx = cos(x)
			opvalue = op.value();
			value = (T)sin(opvalue);
			tmp = (T)cos(opvalue);
			for(unsigned int i=0; i<N; i++) {
				diff[i] = op.diff(i) * tmp;
			}
			break;
		case AD_COS:
			// dcos(x)/dx = -sin(x)
			opvalue = op.value();
			value = (T)cos(opvalue);
			tmp = -(T)sin(opvalue);
			for(unsigned int i=0; i<N; i++) {
				diff[i] = op.diff(i) * tmp;
			}
			break;
		case AD_TAN:
			// dtan(x)/dx = sec(x)^2 = 1/cos(x)^2
			opvalue = op.value();
			value = (T)tan(opvalue); 
			tmp = (T)cos(opvalue);
			tmp = 1.0f / (tmp*tmp);
			for(unsigned int i=0; i<N; i++) {
				diff[i] = op.diff(i) * tmp;
			}
			break;
		case AD_ASIN:
			// dasin(x)/dx = 1/sqrt(1-x^2)
			opvalue = op.value();
			value = asin(opvalue); 
			tmp = (T)(1.0f / sqrt(1.0f-opvalue * opvalue));
			for(unsigned int i=0; i<N; i++) {
				diff[i] = op.diff(i) * tmp;
			}
			break;
		case AD_ACOS:
			// dacos(x)/dx = -1/sqrt(1-x^2)
			opvalue = op.value();
			value = acos(opvalue); 
			tmp = (T)(1.0f / (-sqrt(1.0f-opvalue * opvalue)));
			for(unsigned int i=0; i<N; i++) {
				diff[i] = op.diff(i) * tmp;
			}
			break;
		case AD_ATAN:
			// datan(x)/dx = 1/(1+x^2)
			opvalue = op.value();
			value = atan(opvalue);
			tmp = (T)(1.0f / (1.0f + opvalue*opvalue));
			for(unsigned int i=0; i<N; i++) {
				diff[i] = op.diff(i) * tmp;
			}
			break;
		default:
			AD_ASSERT(false, "should not reach here");
	}
}

template <typename T, unsigned int M>
void ForwardDifferentiate(unsigned int N, ADOpcode opcode, const FADScalar<T, M> &op1, const FADScalar<T, M> &op2, T &value, T *diff) {
	T tmp, logop1;
	switch(opcode) {
		case AD_ADD:
			// (f+g)' = f' + g'
			value = op1.value() + op2.value(); 
			for(unsigned int i=0; i<N; i++) {
				diff[i] = op1.diff(i) + op2.diff(i);
			}
			break;
		case AD_SUB:
			// (f-g)' = f' - g'
			value = op1.value() - op2.value();
			for(unsigned int i=0; i<N; i++) {
				diff[i] = op1.diff(i) - op2.diff(i);
			}
			break;
		case AD_MUL:
			// (f*g)' = f' * g + f * g'
			value = op1.value() * op2.value();
			for(unsigned int i=0; i<N; i++) {
				diff[i] = op1.diff(i)*op2.value() + op1.value()*op2.diff(i);
			}
			break;
		case AD_DIV:
			// (f/g)' = (f' + f/g * g')/g
			value = op1.value() / op2.value();
			for(unsigned int i=0; i<N; i++) {
				diff[i] = (op1.diff(i) - value * op2.diff(i)) / op2.value();
			}
			break;
		case AD_POW:
			// (f^g)' = f ^ (g-1) * (g * f' + f * log(f) * g') = f' * g * f^g / f + g' * f^g * log(f)
			value = (T)pow(op1.value(), op2.value());
			tmp = 0.0f;
			logop1 = (op1.value() != 0.0f) ? (T)log(op1.value()) : 0.0f;
			for(unsigned int i=0; i<N; i++) {
				diff[i] = 0.0f;
				if(op1.diff(i) != 0.0f) {
					diff[i] += op1.diff(i) * op2.value() * value / op1.value();
				}
				if(op2.diff(i) != 0.0f) {
					diff[i] += op2.diff(i) * value * logop1;
				}
			}
			break;
		default:
			AD_ASSERT(false, "should not reach here");
	}
}



typedef FADScalar<double> FADdouble;
typedef FADScalar<float> FADfloat;


#endif

